<!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb-area bg-img bg-overlay2" style="background-image: url(<?=base_url('assets/img/bg-img/breadcumb.jpg');?>);">
        <div class="bradcumbContent">
            <h2><?= $subtitle;?></h2>
        </div>
    </div>
    <!-- bg gradients -->
    <div class="bg-gradients"></div>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### About Us Area Start ##### -->
    <div class="about-us-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <!-- About Thumbnail -->
                <div class="col-12 col-lg-6">
                    <div class="about-thumbnail mb-100">
                        <img src="<?=base_url('assets/img/albums.jpeg');?>" alt="">
                    </div>
                </div>
                <!-- About Content -->
                <div class="col-12 col-lg-6">
                    <div class="about--content mb-100">
                        <h4>Hello, <br>It’s <?= $artist;?></h4>
                        <p>Simpo Savior, original names are:Ndizihiwe Alain Jean Sauveur. Date of birth:29/01/2019. Domicile Rwanda, Kicukiro Kanombe, Kabeza,Giporoso</p>
                        <p>Studies: 1998-2003 primary at Remera Catholic I. 2004-2009 secondary school at Groupe Scolaire Saint Joseph Kabgayi at Muhanga. But 2007 I was at Collège de Gisenyi Inyemeramihigo, 2015-2018 University at UNILAK faculty of law</p>
                        <p>I am singer, songwriter and legal advisor. Political part: Pan African, Modern Conservator </p>
                        

                           <p>I sing about history, politics, and daily life in society.  
Education background: I studied Maths, Biology and Chemistry in secondary school, then I studied law in university means I have degree in law, I like criminal law more than civil law and I like positive discussion. 
I would like to spread whatever I know about African history and heroes to the new generation who are dominated by imperialism, drug, useless conflicts and religious slavery.</p>
<p>  
My idol is Thomas Sankara and others like Cabral Amircal, Lumumba Patrice, Nkwame Nkrouma, Bob Marley, Tiken Jah Fakoly, Nyerere Julius, Rudahigwa Mutara III and others more 
I want to release at least 5 albums in 5 years with God’s will.  
Current work: I work in Motorcycle Tour Company ARAT. </p>
                        <!-- Key Notes -->
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### About Us Area End ##### -->

    <!-- ##### Career Timeline Area Start ##### -->
    <div class="career-timeline-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h2>My Life &amp; career</h2>
                        <h6>Sed porta cursus enim, vitae maximus felis luctus iaculis.</h6>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">

                    <!-- Single Timeline Area -->
                    <div class="single-timeline-area d-flex">
                        <!-- Timeline Date -->
                        <div class="timeline-date">
                            <h2>17 <span>July 1983</span></h2>
                        </div>
                        <!-- Timeline Content -->
                        <div class="timeline-content">
                            <h5>My childhood</h5>
                             <p>Simpo Savior, original names are:Ndizihiwe Alain Jean Sauveur. Date of birth:29/01/2019. Domicile Rwanda, Kicukiro Kanombe, Kabeza,Giporoso</p>
                        <p>Studies: 1998-2003 primary at Remera Catholic I. 2004-2009 secondary school at Groupe Scolaire Saint Joseph Kabgayi at Muhanga. But 2007 I was at Collège de Gisenyi Inyemeramihigo, 2015-2018 University at UNILAK faculty of law</p>
                        <p>I am singer, songwriter and legal advisor. Political part: Pan African, Modern Conservator </p>
                        </div>
                    </div>

                    <!-- Single Timeline Area -->
                    <div class="single-timeline-area d-flex">
                        <!-- Timeline Date -->
                        <div class="timeline-date">
                            <h2>25 <span>May 2004</span></h2>
                        </div>
                        <!-- Timeline Content -->
                        <div class="timeline-content">
                            <h5>Early career years</h5>
                            <p> I write first song when I was in Secondary 2005 and released it, at the end of 2006. Guys then New generation in 2007, With friends where we were thinking to create UGM group (United Guys for Music) where I were with Ndunguye Luck my classmate, Lishirabake Olivier my cousin and Jacques Ndagijimana cousin and more than brother. </p>
                        <p>In 2008 because we were in different areas we begin to stop it because to meet were so complicated. In 2011 I went in Uganda to visit my brother and meet with Djouma Munyanshongore who helped to go back in studio in order to encourage me and went in Power Record of one of big producer in Uganda and made another track profits (go abroad) which is album name, When I come back in Rwanda as I helped by Kamichi I performed at saint Andre on P Fla Launch album then Huye NUR Auditorium at Kamichi' S also </p>
                        </div>
                    </div>

                    <!-- Single Timeline Area -->
                    <div class="single-timeline-area d-flex">
                        <!-- Timeline Date -->
                        <div class="timeline-date">
                            <h2>11 <span>Nov 2019</span></h2>
                        </div>
                        <!-- Timeline Content -->
                        <div class="timeline-content">
                            <h5>First Album Release</h5>
                            <p>I sing about history, politics, and daily life in society.  
Education background: I studied Maths, Biology and Chemistry in secondary school, then I studied law in university means I have degree in law, I like criminal law more than civil law and I like positive discussion. 
I would like to spread whatever I know about African history and heroes to the new generation who are dominated by imperialism, drug, useless conflicts and religious slavery.  
My idol is Thomas Sankara and others like Cabral Amircal, Lumumba Patrice, Nkwame Nkrouma, Bob Marley, Tiken Jah Fakoly, Nyerere Julius, Rudahigwa Mutara III and others more 
I want to release at least 5 albums in 5 years with God’s will.  
Current work: I work in Motorcycle Tour Company ARAT. </p>
                        </div>
                    </div>

                    <!-- Single Timeline Area -->
                    <div class="single-timeline-area d-flex">
                        <!-- Timeline Date -->
                        <div class="timeline-date">
                            <h2>30 <span>May 2009</span></h2>
                        </div>
                        <!-- Timeline Content -->
                        <div class="timeline-content">
                            <h5>My First Idols</h5>
                            <p>  
My idol is Thomas Sankara and others like Cabral Amircal, Lumumba Patrice, Nkwame Nkrouma, Bob Marley, Tiken Jah Fakoly, Nyerere Julius, Rudahigwa Mutara III and others more 
I want to release at least 5 albums in 5 years with God’s will.  
Current work: I work in Motorcycle Tour Company ARAT. </p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="timeline-btn text-center mt-100">
                        <a href="#" class="btn musica-btn">Load More</a>
                    </div>
                </div>
            </div>

        </div>

        <!-- Side Thumbs -->
        <div class="first-img">
            <img src="<?=base_url('assets/img/bg-img/man.png');?>" alt="">
        </div>
        <div class="second-img">
            <img src="<?=base_url('assets/img/bg-img/microphone.png');?>" alt="">
        </div>
    </div>
    <!-- ##### Career Timeline Area End ##### -->

    <!-- ##### Discography Area Start ##### -->
    <div class="discography-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading dark">
                        <h2>discography</h2>
                        <h6>Sed porta cursus enim, vitae maximus felis luctus iaculis.</h6>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <!-- Discography Slides -->
                    <div class="discography-slides owl-carousel">
                        <!-- Single Discography -->
                        <div class="single-discography">
                            <a href="#"><img src="<?=base_url('assets/img/bg-img/d1.jpg');?>" alt=""></a>
                        </div>

                        <!-- Single Discography -->
                        <div class="single-discography">
                            <a href="#"><img src="<?=base_url('assets/img/bg-img/d2.jpg');?>" alt=""></a>
                        </div>

                        <!-- Single Discography -->
                        <div class="single-discography">
                            <a href="#"><img src="<?=base_url('assets/img/bg-img/d3.jpg');?>" alt=""></a>
                        </div>

                        <!-- Single Discography -->
                        <div class="single-discography">
                            <a href="#"><img src="<?=base_url('assets/img/bg-img/d4.jpg');?>" alt=""></a>
                        </div>

                        <!-- Single Discography -->
                        <div class="single-discography">
                            <a href="#"><img src="<?=base_url('assets/img/bg-img/d5.jpg');?>" alt=""></a>
                        </div>

                        <!-- Single Discography -->
                        <div class="single-discography">
                            <a href="#"><img src="<?=base_url('assets/img/bg-img/d5.jpg');?>" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Discography Area End ##### -->

    <!-- ##### CTA Area Start ##### -->
    <div class="musica-cta-area section-padding-100 bg-img bg-overlay2" style="background-image: url(<?=base_url('assets/img/bg-img/bg-8.jpg');?>);">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="cta-content">
                        <div class="cta-text">
                            <span>Unique Way to see a</span>
                            <h2>Music Concert</h2>
                            <h4>Search for the best music</h4>
                        </div>
                        <div class="cta-btn mt-30">
                            <a href="#" class="btn musica-btn">elements</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### CTA Area End ##### -->