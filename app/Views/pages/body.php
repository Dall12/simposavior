<section class="hero-area">
        <div class="hero-slides owl-carousel">

            <!-- Single Hero Slide -->
            <div class="single-hero-slide d-flex align-items-center justify-content-center">
                <!-- Slide Img -->
                <div class="slide-img bg-img" style="background-image: url(<?= base_url('assets/img/bg-img/bg-1.jpg');?>);"></div>
                <!-- Slide Content -->
                <div class="hero-slides-content text-center">
                    <h2 data-animation="fadeInUp" data-delay="100ms">Music <span>Music</span></h2>
                    <p data-animation="fadeInUp" data-delay="300ms">Music Album</p>
                </div>
                <!-- Big Text -->
                <h2 class="big-text">Music</h2>
            </div>

            <!-- Single Hero Slide -->
            <div class="single-hero-slide d-flex align-items-center justify-content-center">
                <!-- Slide Img -->
                <div class="slide-img bg-img" style="background-image: url(<?= base_url('assets/img/bg-img/bg-2.jpg');?>);"></div>
                <!-- Slide Content -->
                <div class="hero-slides-content text-center">
                    <h2 data-animation="fadeInUp" data-delay="100ms">Simpo Savior <span>Simpo Savior</span></h2>
                    <p data-animation="fadeInUp" data-delay="300ms">Music Artist</p>
                </div>
                <!-- Big Text -->
                <h2 class="big-text">Simpo Savior</h2>
            </div>

            <!-- Single Hero Slide -->
            <div class="single-hero-slide d-flex align-items-center justify-content-center">
                <!-- Slide Img -->
                <div class="slide-img bg-img" style="background-image: url(<?= base_url('assets/img/bg-img/bg-3.jpg');?>);"></div>
                <!-- Slide Content -->
                <div class="hero-slides-content text-center">
                    <h2 data-animation="fadeInUp" data-delay="100ms">Festival <span>Festival</span></h2>
                    <p data-animation="fadeInUp" data-delay="300ms">Launch Album</p>
                </div>
                <!-- Big Text -->
                <h2 class="big-text">Festival</h2>
            </div>

        </div>
        <!-- bg gradients -->
        <div class="bg-gradients"></div>

        <!-- Slide Down -->
        <div class="slide-down" id="scrollDown">
            <h6>Slide Down</h6>
            <div class="line"></div>
        </div>
    </section>
    <!-- ##### Hero Area End ##### -->

    <!-- ##### About Us Area Start ##### -->
    <div class="about-us-area section-padding-100-0 bg-img bg-overlay" style="background-image: url(<?= base_url('assets/img/bg-img/bg-4.jpg');?>);" id="about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h2><?= $artist; ?> Bio</h2>
                        <h6>Sed porta cursus enim, vitae maximus felis luctus iaculis.</h6>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- About Thumbnail -->
                <div class="col-12 col-lg-6">
                    <div class="about-thumbnail mb-100">
                        <img src="<?= base_url('assets/img/albums.jpeg');?>" height="470" width="470">
                    </div>
                </div>
                <!-- About Content -->
                <div class="col-12 col-lg-6">
                    <div class="about-content mb-100">
                        <h4>Hello, It’s <?= $artist; ?></h4>
                        <p>Simpo Savior, original names are:Ndizihiwe Alain Jean Sauveur. Date of birth:29/01/2019. Domicile Rwanda, Kicukiro Kanombe, Kabeza,Giporoso</p>
                        <p>Studies: 1998-2003 primary at Remera Catholic I. 2004-2009 secondary school at Groupe Scolaire Saint Joseph Kabgayi at Muhanga. But 2007 I was at Collège de Gisenyi Inyemeramihigo, 2015-2018 University at UNILAK faculty of law</p>
                        <p>I am singer, songwriter and legal advisor. Political part: Pan African, Modern Conservator </p>
                        <p> I write first song when I was in Secondary 2005 and released it, at the end of 2006. Guys then New generation in 2007, With friends where we were thinking to create UGM group (United Guys for Music) where I were with Ndunguye Luck my classmate, Lishirabake Olivier my cousin and Jacques Ndagijimana cousin and more than brother. </p>
                        <p>In 2008 because we were in different areas we begin to stop it because to meet were so complicated. In 2011 I went in Uganda to visit my brother and meet with Djouma Munyanshongore who helped to go back in studio in order to encourage me and went in Power Record of one of big producer in Uganda and made another track profits (go abroad) which is album name, When I come back in Rwanda as I helped by Kamichi I performed at saint Andre on P Fla Launch album then Huye NUR Auditorium at Kamichi' S also </p>
                        <img src="<?= base_url('assets/img/core-img/signature.png');?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### About Us Area End ##### -->

    <!-- ##### Upcoming Shows Area Start ##### -->
    <div class="upcoming-shows-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h2>Upcoming shows</h2>
                        <h6>Sed porta cursus enim, vitae maximus felis luctus iaculis.</h6>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <!-- Upcoming Shows Content -->
                    <div class="upcoming-shows-content">

                        <!-- Single Upcoming Shows -->
                        <div class="single-upcoming-shows d-flex align-items-center flex-wrap">
                            <div class="shows-date">
                                <h2>17 <span>July</span></h2>
                            </div>
                            <div class="shows-desc d-flex align-items-center">
                                <div class="shows-img">
                                    <img src="<?= base_url('assets/img/bg-img/s1.jpg');?>" alt="">
                                </div>
                                <div class="shows-name">
                                    <h6>Electric castle Festival</h6>
                                    <p>Cluj, Romania</p>
                                </div>
                            </div>
                            <div class="shows-location">
                                <p>At the Castle</p>
                            </div>
                            <div class="shows-time">
                                <p>20:30</p>
                            </div>
                            <div class="buy-tickets">
                                <a href="#" class="btn musica-btn">Buy Tikets</a>
                            </div>
                        </div>

                        <!-- Single Upcoming Shows -->
                        <div class="single-upcoming-shows d-flex align-items-center flex-wrap">
                            <div class="shows-date">
                                <h2>23 <span>July</span></h2>
                            </div>
                            <div class="shows-desc d-flex align-items-center">
                                <div class="shows-img">
                                    <img src="<?= base_url('assets/img/bg-img/s2.jpg');?>" alt="">
                                </div>
                                <div class="shows-name">
                                    <h6>Electric Festival</h6>
                                    <p>Manhathan, NY, USA</p>
                                </div>
                            </div>
                            <div class="shows-location">
                                <p>Main Stadium</p>
                            </div>
                            <div class="shows-time">
                                <p>21:30</p>
                            </div>
                            <div class="buy-tickets">
                                <a href="#" class="btn musica-btn">Buy Tikets</a>
                            </div>
                        </div>

                        <!-- Single Upcoming Shows -->
                        <div class="single-upcoming-shows d-flex align-items-center flex-wrap">
                            <div class="shows-date">
                                <h2>25 <span>July</span></h2>
                            </div>
                            <div class="shows-desc d-flex align-items-center">
                                <div class="shows-img">
                                    <img src="<?= base_url('assets/img/bg-img/s3.jpg');?>" alt="">
                                </div>
                                <div class="shows-name">
                                    <h6>Sunflower festival</h6>
                                    <p>Paris, France</p>
                                </div>
                            </div>
                            <div class="shows-location">
                                <p>Sunflower Arena</p>
                            </div>
                            <div class="shows-time">
                                <p>20:30</p>
                            </div>
                            <div class="buy-tickets">
                                <a href="#" class="btn musica-btn">Buy Tikets</a>
                            </div>
                        </div>

                        <!-- Single Upcoming Shows -->
                        <div class="single-upcoming-shows d-flex align-items-center flex-wrap">
                            <div class="shows-date">
                                <h2>30 <span>July</span></h2>
                            </div>
                            <div class="shows-desc d-flex align-items-center">
                                <div class="shows-img">
                                    <img src="<?= base_url('assets/img/bg-img/s4.jpg');?>" alt="">
                                </div>
                                <div class="shows-name">
                                    <h6>Electric castle Festival</h6>
                                    <p>Cluj, Romania</p>
                                </div>
                            </div>
                            <div class="shows-location">
                                <p>At the Castle</p>
                            </div>
                            <div class="shows-time">
                                <p>20:30</p>
                            </div>
                            <div class="buy-tickets">
                                <a href="#" class="btn musica-btn">Buy Tikets</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Upcoming Shows Area End ##### -->

    <!-- ##### Music Player Area Start ##### -->
    <div class="music-player-area section-padding-100" >
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="music-player-slides owl-carousel">

                        <!-- Single Music Player -->
                        <div class="single-music-player">
                            <img src="<?= base_url('assets/img/album.jpeg');?>" alt="">

                            <div class="music-info d-flex justify-content-between">
                                <div class="music-text">
                                    <h5>Simpo Savior</h5>
                                    <p>Guys</p>
                                </div>
                                <div class="music-play-icon">
                                    <audio preload="auto" controls>
                                    <source src="<?= base_url('assets/audio/Simpo-Savior-Guys.mp3');?>">
                                </audio>
                                </div>
                            </div>
                        </div>

                        <!-- Single Music Player -->
                        <div class="single-music-player">
                            <img src="<?= base_url('assets/img/album.jpeg');?>" alt="">

                            <div class="music-info d-flex justify-content-between">
                                <div class="music-text">
                                    <h5>Simpo Savior</h5>
                                    <p>Life is Jah's Gift</p>
                                </div>
                                <div class="music-play-icon">
                                    <audio preload="auto" controls>
                                    <source src="<?= base_url('assets/audio/Simpo-Savior-Life-is-Jahs-gift.mp3');?>">
                                </audio>
                                </div>
                            </div>
                        </div>

                        <!-- Single Music Player -->
                        <div class="single-music-player">
                            <img src="<?= base_url('assets/img/album.jpeg');?>" alt="">

                            <div class="music-info d-flex justify-content-between">
                                <div class="music-text">
                                    <h5>Simpo Savior</h5>
                                    <p>New Generation</p>
                                </div>
                                <div class="music-play-icon">
                                    <audio preload="auto" controls>
                                    <source src="<?= base_url('assets/audio/Simpo-Savior-New-Generation.mp3');?>">
                                </audio>
                                </div>
                            </div>
                        </div>

                        <!-- Single Music Player -->
                        <div class="single-music-player">
                            <img src="<?= base_url('assets/img/album.jpeg');?>" alt="">

                            <div class="music-info d-flex justify-content-between">
                                <div class="music-text">
                                    <h5>Simpo Savior</h5>
                                    <p>Africa we are the one</p>
                                </div>
                                <div class="music-play-icon">
                                    <audio preload="auto" controls>
                                    <source src="<?= base_url('assets/audio/Simpo-Savior-Africa-We-Are-The-One.mp3');?>">
                                </audio>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Music Player Area End ##### -->

    <!-- ##### Featured Album Area Start ##### -->
    <div class="featured-album-area section-padding-100 clearfix" id="album">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="featured-album-content d-flex flex-wrap">

                        <!-- Album Thumbnail -->
                        <div class="album-thumbnail h-100 bg-img" style="background-image: url(<?= base_url('assets/img/album.jpeg');?>);"></div>

                        <!-- Album Songs -->
                        <div class="album-songs h-100">

                            <!-- Album Info -->
                            <div class="album-info mb-50 d-flex flex-wrap align-items-center justify-content-between">
                                <div class="album-title">
                                    <h6>AFRICA</h6>
                                    <h4>Why do your profits go aboard?</h4>
                                </div>
                                <div class="album-buy-now">
                                    <a href="#" class="btn musica-btn">Listen Album</a>
                                </div>
                            </div>

                            <div class="album-all-songs">

                                <!-- Music Playlist -->
                                <div class="music-playlist">
                                    <!-- Single Song -->
                                    
                                    <div class="single-music active">
                                        <h6> Africa, we are one</h6>
                                        <audio preload="auto" controls>
                                            <source src="<?= base_url('assets/audio/Simpo-Savior-Africa-We-Are-The-One.mp3');?>">
                                        </audio>
                                    </div>

                                    <!-- Single Song -->
                                    <div class="single-music">
                                        <h6> Never forget them (African heroes) </h6>
                                        <audio preload="auto" controls>
                                            <source src="<?= base_url('assets/audio/Simpo-Savior-Never-Forget-Them(African-Heroes).mp3');?>">
                                        </audio>
                                    
                                    </div>

                                    <!-- Single Song -->
                                    <div class="single-music">
                                        <h6>Amahanga arahanda </h6>
                                        <audio preload="auto" controls>
                                            <source src="<?= base_url('assets/audio/Simpo-Savior-Amahanga-Arahanda.mp3');?>">
                                        </audio>
                                    </div>

                                    <!-- Single Song -->
                                    <div class="single-music">
                                        <h6> Profits go abroad </h6>
                                        <audio preload="auto" controls>
                                            <source src="<?= base_url('assets/audio/Simpo-Savior-Profits-Go-Abroad.mp3');?>">
                                        </audio>                                  
                                    </div>

                                    <!-- Single Song -->
                                    <div class="single-music">
                                        <h6>Pas de guerre </h6>
                                        <audio preload="auto" controls>
                                            <source src="<?= base_url('assets/audio/Simpo-Savior-Pas-de-guerre(Official-audio).mp3');?>">
                                        </audio>
                                    </div>

                                    <!-- Single Song -->
                                    <div class="single-music">
                                        <h6>Turn around</h6>
                                       <audio preload="auto" controls>
                                            <source src="<?= base_url('assets/audio/Simpo-Savior-Turn-Around.mp3');?>">
                                        </audio>
                                    </div>
                                    <!-- Single Song -->
                                    <div class="single-music">
                                        <h6> New generation </h6>
                                        <audio preload="auto" controls>
                                            <source src="<?= base_url('assets/audio/Simpo-Savior-New-Generation.mp3');?>">
                                        </audio>
                                    </div>

                                    <!-- Single Song -->
                                    <div class="single-music">
                                        <h6>Kamali Kamanzi </h6>
                                        <audio preload="auto" controls>
                                            <source src="<?= base_url('assets/audio/Simpo-Savior-Kamali-Kamanzi.mp3');?>">
                                        </audio>
                                    </div>

                                    <!-- Single Song -->
                                    <div class="single-music">
                                        <h6> Guys </h6>
                                        <audio preload="auto" controls>
                                            <source src="<?= base_url('assets/audio/Simpo-Savior-Guys.mp3');?>">
                                        </audio>
                                    </div>

                                    <!-- Single Song -->
                                    <div class="single-music">
                                        <h6> Life is Jah’s gift </h6>
                                        <audio preload="auto" controls>
                                            <source src="<?= base_url('assets/audio/Simpo-Savior-Life-is-Jahs-gift.mp3');?>">
                                        </audio>
                                    </div>

                                    <!-- Single Song -->
                                    <div class="single-music">
                                        <h6> Don’t be lazy </h6>
                                        <audio preload="auto" controls>
                                            <source src="<?= base_url('assets/audio/Simpo-Savior-Dont-be-lazy.mp3');?>">
                                        </audio>
                                    </div>

                                    <!-- Single Song -->
                                    
                                </div>
                            </div>

                            <!-- Now Playing -->
                            <div class="now-playing d-flex flex-wrap align-items-center justify-content-between">
                                <div class="songs-name">
                                    <p>Playing</p>
                                    <h6> Africa, we are one</h6>
                                </div>
                                <audio preload="auto" controls>
                                    <source src="<?= base_url('assets/audio/Simpo-Savior-Africa-We-Are-The-One.mp3');?>">
                                </audio>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Featured Album Area End ##### -->

    <!-- ##### Music Artists Area Start ##### -->
    <div class="musica-music-artists-area d-flex flex-wrap clearfix">
        <!-- Music Search -->
        <div class="music-search bg-img bg-overlay2 wow fadeInUp" data-wow-delay="300ms" style="background-image: url(<?= base_url('assets/img/bg-img/bg-9.jpg');?>);">
            <!-- Content -->
            <div class="music-search-content">
                <h2>Music</h2>
                <h4>Search for the best music</h4>
            </div>
        </div>

        <!-- Artists Search -->
        <div class="artists-search bg-img bg-overlay2 wow fadeInUp" data-wow-delay="600ms" style="background-image: url(<?= base_url('assets/img/bg-img/bg-1.jpg');?>);">
            <!-- Content -->
            <div class="music-search-content">
                <h2>Artists</h2>
                <h4>Search for the best artists</h4>
            </div>
        </div>
    </div>
    <!-- ##### Music Artists Area End ##### -->