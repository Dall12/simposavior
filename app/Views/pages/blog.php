<div class="breadcumb-area bg-img bg-overlay2" style="background-image: url(<?=base_url('assets/img/bg-img/breadcumb3.jpg');?>)">
        <div class="bradcumbContent">
            <h2><?= $subtitle;?></h2>
        </div>
    </div>
    <!-- bg gradients -->
    <div class="bg-gradients"></div>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Blog Area Start ##### -->
    <div class="blog-area mt-30 section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="fitness-blog-posts">

                        <!-- Single Post Start -->
                        <div class="single-blog-post mb-100 wow fadeInUp" data-wow-delay="100ms">
                            <!-- Post Thumb -->
                            <div class="blog-post-thumb mb-30">
                                <img src="<?=base_url('assets/img/blog-img/1.jpg');?>" alt="">
                            </div>
                            <!-- Post Title -->
                            <a href="#" class="post-title">10 Best Festival that you should’t miss this summer</a>
                            <!-- Post Meta -->
                            <div class="post-meta d-flex justify-content-between">
                                <div class="post-date">
                                    <p>May 22, 2018</p>
                                </div>
                                <!-- Comments -->
                                <p class="comments"><a href="#">3 comments</a></p>
                            </div>
                            <!-- bg gradients -->
                            <div class="bg-gradients mb-30 w-25"></div>
                            <!-- Post Excerpt -->
                            <p>Sed dapibus varius massa vel auctor. Nulla massa dui, posuere non erat in, eleifend mattis dui. Vivamus luctus luctus rhoncus. Donec sagittis nulla id finibus iaculis. Mauris odio tortor, suscipit non elit ut, imperdiet ornare erat. Vestibulum vel lorem eget risus pulvinar sollicitudin in a erat. Quisque mattis ultricies arcu, ac venenatis nisl. Sed dapibus varius massa vel auctor. Nulla massa dui, posuere non erat in, eleifend mattis dui. Vivamus luctus luctus rhoncus. Donec sagittis nulla id finibus iaculis.</p>
                            <!-- Read More -->
                            <a href="#" class="read-more-btn">Read more</a>
                        </div>

                        <!-- Single Post Start -->
                        <div class="single-blog-post mb-100 wow fadeInUp" data-wow-delay="300ms">
                            <!-- Post Thumb -->
                            <div class="blog-post-thumb mb-30">
                                <img src="<?=base_url('assets/img/blog-img/2.jpg');?>" alt="">
                            </div>
                            <!-- Post Title -->
                            <a href="#" class="post-title">10 Best Festival that you should’t miss this summer</a>
                            <!-- Post Meta -->
                            <div class="post-meta d-flex justify-content-between">
                                <div class="post-date">
                                    <p>May 22, 2018</p>
                                </div>
                                <!-- Comments -->
                                <p class="comments"><a href="#">3 comments</a></p>
                            </div>
                            <!-- bg gradients -->
                            <div class="bg-gradients mb-30 w-25"></div>
                            <!-- Post Excerpt -->
                            <p>Sed dapibus varius massa vel auctor. Nulla massa dui, posuere non erat in, eleifend mattis dui. Vivamus luctus luctus rhoncus. Donec sagittis nulla id finibus iaculis. Mauris odio tortor, suscipit non elit ut, imperdiet ornare erat. Vestibulum vel lorem eget risus pulvinar sollicitudin in a erat. Quisque mattis ultricies arcu, ac venenatis nisl. Sed dapibus varius massa vel auctor. Nulla massa dui, posuere non erat in, eleifend mattis dui. Vivamus luctus luctus rhoncus. Donec sagittis nulla id finibus iaculis.</p>
                            <!-- Read More -->
                            <a href="#" class="read-more-btn">Read more</a>
                        </div>

                        <!-- Single Post Start -->
                        <div class="single-blog-post mb-100 wow fadeInUp" data-wow-delay="500ms">
                            <!-- Post Thumb -->
                            <div class="blog-post-thumb mb-30">
                                <img src="<?=base_url('assets/img/blog-img/3.jpg');?>" alt="">
                            </div>
                            <!-- Post Title -->
                            <a href="#" class="post-title">10 Best Festival that you should’t miss this summer</a>
                            <!-- Post Meta -->
                            <div class="post-meta d-flex justify-content-between">
                                <div class="post-date">
                                    <p>May 22, 2018</p>
                                </div>
                                <!-- Comments -->
                                <p class="comments"><a href="#">3 comments</a></p>
                            </div>
                            <!-- bg gradients -->
                            <div class="bg-gradients mb-30 w-25"></div>
                            <!-- Post Excerpt -->
                            <p>Sed dapibus varius massa vel auctor. Nulla massa dui, posuere non erat in, eleifend mattis dui. Vivamus luctus luctus rhoncus. Donec sagittis nulla id finibus iaculis. Mauris odio tortor, suscipit non elit ut, imperdiet ornare erat. Vestibulum vel lorem eget risus pulvinar sollicitudin in a erat. Quisque mattis ultricies arcu, ac venenatis nisl. Sed dapibus varius massa vel auctor. Nulla massa dui, posuere non erat in, eleifend mattis dui. Vivamus luctus luctus rhoncus. Donec sagittis nulla id finibus iaculis.</p>
                            <!-- Read More -->
                            <a href="#" class="read-more-btn">Read more</a>
                        </div>

                        <!-- Pagination -->
                        <div class="musica-pagination-area wow fadeInUp" data-wow-delay="700ms">
                            <nav>
                                <ul class="pagination">
                                    <li class="page-item active"><a class="page-link" href="#">01.</a></li>
                                    <li class="page-item"><a class="page-link" href="#">02.</a></li>
                                    <li class="page-item"><a class="page-link" href="#">03.</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Blog Area End ##### -->

    <!-- ##### CTA Area Start ##### -->
    <div class="musica-cta-area section-padding-100 bg-img bg-overlay2" style="background-image: url(<?=base_url('assets/img/blog-img/4.jpg');?>);">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="cta-content d-flex justify-content-between align-items-center">
                        <div class="cta-text">
                            <h4>Contact us now</h4>
                            <h2>Do you have a question?</h2>
                            <h6>Morbi quis venenatis augue, a tincidunt libero. Sed id porttitor elit, eu ultricies mauris.</h6>
                        </div>
                        <div class="cta-btn">
                            <a href="#" class="btn musica-btn">contact us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>