<?php namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$data['title'] = "Simpo Savior";
		$data['subtitle'] = "Music";
		$data['artist'] = "Simpo Savior";
		$data['content'] = view("pages/body", $data);
		return view('main', $data);
	}

	public function music(){

		$data['title'] = "Simpo Savior";
		$data['subtitle'] = "Music";
		$data['artist'] = "Simpo Savior";
		$data['content'] = view("pages/music", $data);
		return view('main', $data);
	}

	public function biography(){

		$data['title'] = "Simpo Savior";
		$data['subtitle'] = "Biography";
		$data['artist'] = "Simpo Savior";
		$data['content'] = view("pages/biography", $data);
		return view('main', $data);
	}

	public function blog(){

		$data['title'] = "Simpo Savior";
		$data['subtitle'] = "Tours and Concert";
		$data['artist'] = "Simpo Savior";
		$data['content'] = view("pages/blog", $data);
		return view('main', $data);
	}



	//--------------------------------------------------------------------

}
